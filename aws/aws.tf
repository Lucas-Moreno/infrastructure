provider "aws" {
  region     = "eu-west-3"
  access_key = data.vault_generic_secret.aws_secrets.data["value"]
  secret_key = data.vault_generic_secret.aws_secret.data["value"]
}

provider "vault" {
  address = "http://127.0.0.1:8200"
}

data "vault_generic_secret" "aws_secrets" {
  path = "secret/aws_access_key_id"
}

data "vault_generic_secret" "aws_secret" {
  path = "secret/aws_secret_access_key"
}

resource "aws_instance" "example" {
  ami           = "ami-0f014d1f920a73971"
  instance_type = "t2.micro"

  tags = {
    Name = "t-clo-vm"
  }
}

output "instance_ip" {
  value = aws_instance.example.public_ip
}

output "ssh_user" {
  value = "ec2-user"
}
